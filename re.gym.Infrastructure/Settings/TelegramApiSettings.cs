﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace re.gym.Infrastructure.Settings
{
    public class TelegramApiSettings
    {
        public string BotName { get; set; }

        public string HttpAccessToken { get; set; }
    }
}
