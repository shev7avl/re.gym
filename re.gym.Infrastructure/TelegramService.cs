﻿using Microsoft.Extensions.Hosting;
using re.gym.Infrastructure.Settings;
using Telegram.Bot;

namespace re.gym.Infrastructure
{
    //TODO: Впилить телеграм бота
    public class TelegramService: BackgroundService, ITelegramService
    {
        private readonly TelegramBotClient _telegramBotClient;
        private readonly TelegramApiSettings _settings;

        public TelegramService(TelegramApiSettings settings)
        {
            _settings = settings;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.Run(() => Thread.Sleep(3000), stoppingToken);
        }
    }
}
