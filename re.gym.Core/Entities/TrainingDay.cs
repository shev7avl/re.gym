﻿using re.gym.Core.Abstract;

namespace re.gym.Core.Entities
{
    /// <summary>
    /// Класс описывающий тренировочный день
    /// </summary>
    public class TrainingDay: BaseEntity
    {
        /// <summary>
        /// Номер недели
        /// </summary>
        public int WeekNumber { get; set; }

        /// <summary>
        /// День недели
        /// </summary>
        public TrainingDayType TrainingDayType { get; set; }

        /// <summary>
        /// Перечисление упражнений
        /// </summary>
        public IEnumerable<int> RoutineIds { get; set; }

    }
}
