﻿using re.gym.Core.Abstract;

namespace re.gym.Core.Entities
{
    /// <summary>
    /// Запись об упражнении
    /// </summary>
    public class Exercise: BaseEntity
    {
        /// <summary>
        /// Название упражнения
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Тип расчетного веса в упражнении
        /// </summary>
        public ExerciseWeightType WeightType { get; set; }
    }
}
