﻿namespace re.gym.Core.Entities
{
    /// <summary>
    /// Тип расчетного веса в упражнении
    /// </summary>
    public enum ExerciseWeightType
    {
        /// <summary>
        /// Фиксированный
        /// </summary>
        FixedWeight,

        /// <summary>
        /// Процент от 1ПМ
        /// </summary>
        PercentOfMaximum,

        /// <summary>
        /// Упражнение без веса
        /// </summary>
        NoWeight
    }
}
