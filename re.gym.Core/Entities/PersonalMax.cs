﻿using re.gym.Core.Abstract;

namespace re.gym.Core.Entities
{
    /// <summary>
    /// Запись о личном рекорде в упражнении
    /// </summary>
    public class PersonalMax: BaseEntity
    {
        /// <summary>
        /// ID упражнения
        /// </summary>
        public int ExerciseId { get; set; }

        /// <summary>
        /// Вес
        /// </summary>
        public float Weight { get; set; }

        /// <summary>
        /// Число повторений
        /// </summary>
        public int Reps { get; set; }

        /// <summary>
        /// Дата установления
        /// </summary>
        public DateTime Date { get; set; }
    }
}
