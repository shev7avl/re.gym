﻿using re.gym.Core.Abstract;

namespace re.gym.Core.Entities
{
    /// <summary>
    /// Упражнение в тренировочном дне (наименование, вес, подходы, повторения)
    /// </summary>
    public class Routine: BaseEntity
    {
        /// <summary>
        /// ID упражнения
        /// </summary>
        public int ExerciseId { get; set; }

        /// <summary>
        /// Вес в упражнении
        /// </summary>
        public float Weight { get; set; }

        /// <summary>
        /// Количество подходов
        /// </summary>
        public int Sets { get; set; }

        /// <summary>
        /// Количество повторений
        /// </summary>
        public int Reps { get; set; }
    }
}
