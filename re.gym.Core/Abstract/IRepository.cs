﻿namespace re.gym.Core.Abstract
{
    public interface IRepository<T> where T: BaseEntity
    {
        public T Get(int id);
        public IEnumerable<T> GetAll();
        public IEnumerable<T> GetRange(IEnumerable<int> ids);
        public void Add(T entity);
        public void AddRange(IEnumerable<T> entities);
        public void Update(T entity);
        public void UpdateRange(IEnumerable<T> entities);
        public void Delete(int id);
        public void DeleteRange(IEnumerable<int> ids);


        public Task<T> GetAsync(int id, CancellationToken cancellationToken);
        public Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken);
        public Task<IEnumerable<T>> GetRangeAsync(IEnumerable<int> ids, CancellationToken cancellationToken);
        public Task AddAsync(T entity, CancellationToken cancellationToken);
        public Task AddRangeAsync(IEnumerable<T> entities, CancellationToken cancellationToken);
        public Task UpdateAsync(T entity, CancellationToken cancellationToken);
        public Task UpdateRangeAsync(IEnumerable<T> entities, CancellationToken cancellationToken);
        public Task DeleteAsync(int id, CancellationToken cancellationToken);
        public Task DeleteRangeAsync(IEnumerable<int> ids, CancellationToken cancellationToken);
    }
}
