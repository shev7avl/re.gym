﻿using AutoMapper;
using re.gym.Application.Resolvers;
using re.gym.Application.View;
using re.gym.Core.Entities;

namespace re.gym.Tests.Application.Resolvers
{
    [TestFixture]
    public class PersonalMaxResolverTests
    {
        private IMapper _mapper;

        [OneTimeSetUp]
        public void Setup()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PersonalMaxView, PersonalMax>()
                    .ForMember(dest => dest.Weight, options => options.MapFrom<PersonalMaxWeightResolver>())
                    .ForMember(dest => dest.Reps, options => options.MapFrom(src => 1))
                    .ReverseMap()
                    .ForMember(dest => dest.Weight, options => options.MapFrom<ReversePersonalMaxWeightResolver>())
                    .ForMember(dest => dest.Reps, options => options.MapFrom(src => src.Reps));
            });

            _mapper = config.CreateMapper();
        }

        [Test]
        [TestCase(35, 15, 50)]
        [TestCase(24, 10, 32.5f)]
        [TestCase(100, 8, 125)]
        public void PersonalMaxResolver_IfPassedValidValues_ReturnValidWeight(float weight, int reps, float expectedWeight)
        {
            var pmView = new PersonalMaxView
            {
                Id = 1,
                ExcerciseId = 1,
                Weight = weight,
                Reps = reps,
                Date = DateTime.Now,
            };

            var pm = _mapper.Map<PersonalMax>(pmView);

            Assert.Multiple(() =>
            {
                Assert.That(pm.Weight, Is.EqualTo(expectedWeight));
                Assert.That(pm.Reps, Is.EqualTo(1));
            });
        }

        [Test]
        [TestCase(50, 15, 35)]
        [TestCase(80, 10, 60)]
        [TestCase(125, 8, 100)]
        public void PersonalMaxResolver_IfPassedValidReps_ReturnValidWeight(float weight, int reps, float expectedWeight)
        {
            var personalMax = new PersonalMax
            {
                Id = 1,
                ExerciseId = 1,
                Weight = weight,
                Reps = 1,
                Date = DateTime.Now,
            };

            personalMax.Reps = reps;

            var pmView = _mapper.Map<PersonalMaxView>(personalMax);

            Assert.Multiple(() =>
            {
                Assert.That(pmView.Weight, Is.EqualTo(expectedWeight));
                Assert.That(pmView.Reps, Is.EqualTo(reps));
            });
        }
    }
}
