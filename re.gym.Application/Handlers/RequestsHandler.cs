﻿using re.gym.Core.Abstract;
using re.gym.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace re.gym.Application.Handlers
{
    public class RequestsHandler<TModel, TView> : IHandler<TModel, TView> where TModel : BaseEntity where TView : class
    {
        private readonly IRepository<TModel> _repository;
        private readonly IMapper _mapper;

        public RequestsHandler(IMapper mapper, IRepository<TModel> repository)
        {
            _repository= repository;
            _mapper= mapper;
        }

        public async Task<TModel> Add(TView view)
        {
            var excercise = _mapper.Map<TModel>(view);
            await _repository.AddAsync(excercise, new CancellationToken());

            return excercise;
        }

        public async Task<IEnumerable<TModel>> AddRange(IEnumerable<TView> views)
        {
            var models = _mapper.Map<List<TModel>>(views);
            await _repository.AddRangeAsync(models, new CancellationToken());

            return models;
        }

        public async Task Delete(int id)
        {
            var ct = new CancellationToken();
            var notFound = await _repository.GetAsync(id, ct) is null;
            if (notFound)
                throw new KeyNotFoundException();

            await _repository.DeleteAsync(id, ct);
        }

        public async Task<TView> GetById(int id)
        {
            var ct = new CancellationToken();
            var model = await _repository.GetAsync(id, ct);
            if (model is null)
                throw new KeyNotFoundException();

            var view = _mapper.Map<TView>(model);
            return view;
        }

        public async Task<IEnumerable<TView>> GetRange(IEnumerable<int> ids)
        {
            var ct = new CancellationToken();
            if (ids != null && ids.Any())
            {
                var result = await _repository.GetRangeAsync(ids, ct);

                if (result is null)
                    throw new KeyNotFoundException();
                var views = _mapper.Map<List<TView>>(result);

                return views;
            }
            else
            {
                var result = await _repository.GetAllAsync(ct);
                if (result is null)
                    throw new KeyNotFoundException();

                var views = _mapper.Map<List<TView>>(result);
                return views;
            }
        }

        public async Task Update(int id, TView view)
        {
            var cancellationToken = new CancellationToken();

            var oldModel = await _repository.GetAsync(id, cancellationToken);
            if (oldModel is null)
                throw new KeyNotFoundException();

            _mapper.Map(view, oldModel);

            await _repository.UpdateAsync(oldModel, new CancellationToken());
        }
        
    }
}
