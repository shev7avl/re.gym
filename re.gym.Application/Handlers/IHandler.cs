﻿using re.gym.Core.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace re.gym.Application.Handlers
{
    public interface IHandler<TModel, TView> where TModel : BaseEntity where TView : class
    {
        Task<TModel> Add(TView view);
        Task<IEnumerable<TModel>> AddRange(IEnumerable<TView> views);
        Task Update (int id, TView view);
        Task Delete(int id);
        Task<TView> GetById(int id);
        Task<IEnumerable<TView>> GetRange(IEnumerable<int> ids);
    }
}
