﻿using AutoMapper;
using re.gym.Application.View;
using re.gym.Core.Entities;

namespace re.gym.Application.Resolvers
{
    public class PersonalMaxWeightResolver : IValueResolver<PersonalMaxView, PersonalMax, float>
    {
        public float Resolve(PersonalMaxView source, PersonalMax destination, float destMember, ResolutionContext context)
        {
            if (source.Reps > 1)
                return RoundToLowestPlate(RecalculateWeightBasedOnReps(source.Weight, source.Reps));

            return source.Weight;
        }

        private static float RoundToLowestPlate(float weight)
        {
            return (float)Math.Round((double)weight / 2.5d, MidpointRounding.AwayFromZero) * 2.5f;
        }

        private static float RecalculateWeightBasedOnReps(float weight, int reps)
        {
            return reps switch
            {
                1 => weight,
                2 or 3 => (float)(weight / 0.95),
                4 or 5 => (float)(weight / 0.9),
                6 or 7 => (float)(weight / 0.85),
                8 or 9 => (float)(weight / 0.8),
                10 or 11 => (float)(weight / 0.75),
                12 or 13 or 14 or 15 or 16 or 17 => (float)(weight / 0.7),
                18 or 19 or 20 or 21 or 22 or 23 or 24 or 25 => (float)(weight / 0.65),
                _ => (float)(weight / 0.6),
            };
        }
    }

    public class ReversePersonalMaxWeightResolver : IValueResolver<PersonalMax, PersonalMaxView, float>
    {
        public float Resolve(PersonalMax source, PersonalMaxView destination, float destMember, ResolutionContext context)
        {
            if (source.Reps > 1)
                return RoundToLowestPlate(RecalculateWeightBasedOnReps(source.Weight, source.Reps));

            return source.Weight;
        }

        private static float RoundToLowestPlate(float weight)
        {
            return (float)Math.Round((double)weight / 2.5d, MidpointRounding.AwayFromZero) * 2.5f;
        }

        private static float RecalculateWeightBasedOnReps(float weight, int reps)
        {
            return reps switch
            {
                1 => weight,
                2 or 3 => (float)(weight * 0.95),
                4 or 5 => (float)(weight * 0.9),
                6 or 7 => (float)(weight * 0.85),
                8 or 9 => (float)(weight * 0.8),
                10 or 11 => (float)(weight * 0.75),
                12 or 13 or 14 or 15 or 16 or 17 => (float)(weight * 0.7),
                18 or 19 or 20 or 21 or 22 or 23 or 24 or 25 => (float)(weight * 0.65),
                _ => (float)(weight * 0.6),
            };
        }
    }

}
