﻿using re.gym.Core.Abstract;
using re.gym.Core.Entities;

namespace re.gym.Application.View
{
    public class ExerciseView: ViewBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ExerciseWeightType Type { get; set; }
    }
}
