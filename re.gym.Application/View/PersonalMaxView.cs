﻿using re.gym.Core.Abstract;

namespace re.gym.Application.View
{
    public class PersonalMaxView: ViewBase
    {
        public int ExcerciseId { get; set; }
        public float Weight { get; set; }
        public int Reps { get; set; }
        public DateTime Date { get; set; }
    }
}
