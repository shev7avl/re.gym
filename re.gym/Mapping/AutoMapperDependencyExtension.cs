﻿using AutoMapper;
using re.gym.Application.Resolvers;
using re.gym.Application.View;
using re.gym.Core.Entities;

namespace re.gym.Web.Mapping
{
    public static class AutoMapperDependencyExtension
    {
        public static void AddAutoMapperDependency(this IServiceCollection services)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ExerciseView, Exercise>()
                    .ReverseMap();
                cfg.CreateMap<PersonalMaxView, PersonalMax>()
                    .ForMember(dest => dest.Weight, options => options.MapFrom<PersonalMaxWeightResolver>())
                    .ForMember(dest => dest.Reps, options => options.MapFrom(src => 1))
                    .ReverseMap()
                    .ForMember(dest => dest.Weight, options => options.MapFrom<ReversePersonalMaxWeightResolver>())
                    .ForMember(dest => dest.Reps, options => options.MapFrom(src => src));
            });

            services.AddSingleton<IMapper>(config.CreateMapper());
        }
    }
}
