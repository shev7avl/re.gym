using Microsoft.EntityFrameworkCore;
using re.gym.Application.Handlers;
using re.gym.Core.Abstract;
using re.gym.DAL.Context;
using re.gym.DAL.Repository;
using re.gym.Infrastructure.Settings;
using re.gym.Web.Mapping;

namespace re.gym
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddDbContext<DatabaseContext>(options => options.UseSqlite(@"Filename=.\re.gym.DB.sqlite"));
            builder.Services.AddTransient(typeof(IRepository<>), typeof(SqliteRepository<>));
            builder.Services.AddAutoMapperDependency();
            builder.Services.AddTransient(typeof(IHandler<,>), typeof(RequestsHandler<,>));

            builder.Services.Configure<TelegramApiSettings>(builder.Configuration.GetSection(nameof(TelegramApiSettings)));

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}