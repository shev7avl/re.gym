﻿using Microsoft.AspNetCore.Mvc;
using re.gym.Core.Entities;
using re.gym.Application.View;
using re.gym.Application.Handlers;

namespace re.gym.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PersonalMaxController : ControllerBase
    {
        private readonly IHandler<PersonalMax, PersonalMaxView> _handler;

        public PersonalMaxController(IHandler<PersonalMax, PersonalMaxView> handler)
        {
            _handler = handler;
        }

        // Add Personal Max for Exercise with Date
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddPersonalMax([FromBody] PersonalMaxView view)
        {
            try
            {
                var result = await _handler.Add(view);

                return CreatedAtAction(nameof(AddPersonalMax), new { id = result.Id }, result);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // Edit Personal Max for Exercise
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> EditPersonalMax(int id, [FromBody] PersonalMaxView updateView)
        {
            try
            {
                await _handler.Update(id, updateView);

                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // Get History of All Personal Maxes by excerciseId
        [HttpGet("{excerciseId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAllPersonalMaxesByExcerciseId(int excerciseId)
        {
            try
            {
                var personalMaxes = await _handler.GetRange(new List<int>());
                var result = personalMaxes.Where(o => o.ExcerciseId == excerciseId).ToList();
                return Ok(personalMaxes);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // Get History of All Personal Maxes
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllPersonalMaxes()
        {
            try
            {
                var personalMaxes = await _handler.GetRange(new List<int>());
                return Ok(personalMaxes);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // Delete History of All Personal Maxes
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> DeletePersonalMax(int id)
        {
            try
            {
                await _handler.Delete(id);
                return NoContent();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }

}
