﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using re.gym.Application.Handlers;
using re.gym.Application.View;
using re.gym.Core.Abstract;
using re.gym.Core.Entities;

namespace re.gym.Web.Controllers
{
    /// <summary>
    /// Контроллер для упражнений
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class ExerciseController: ControllerBase
    {
        private readonly IHandler<Exercise, ExerciseView> _handler;

        public ExerciseController(IHandler<Exercise, ExerciseView> handler)
        {
            _handler = handler;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddExercise([FromBody] ExerciseView view)
        {
            try
            {
                var result = await _handler.Add(view);
                return CreatedAtAction(nameof(AddExercise), new { id = result.Id }, result);
            }
            catch (Exception)
            {
                return BadRequest();
            }    
        }

        [HttpPost("batch")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddExercise([FromBody] IEnumerable<ExerciseView> views)
        {
            try
            {
                var result = await _handler.AddRange(views);
                return CreatedAtAction(nameof(AddExercise), new { id = result.Select(e => e.Id)}, result);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> EditExercise(int id, [FromBody] ExerciseView view)
        {
            try
            {
                await _handler.Update(id, view);
                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteExercise(int id)
        {
            try
            {
                await _handler.Delete(id);
                return NoContent();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ExerciseView>> GetExercise(int id)
        {
            var result = await _handler.GetById(id);

            return Ok(result);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<ExerciseView>>> GetExerciseRange([FromQuery] IEnumerable<int> ids)
        {
            try
            {
                var result = _handler.GetRange(ids);
                return Ok(result);
            }
            catch (KeyNotFoundException)
            { 
                return NotFound(); 
            }
            catch (Exception)
            {
                return BadRequest();
            } 
        }

    }
}
