﻿using Microsoft.EntityFrameworkCore;
using re.gym.Core.Entities;

namespace re.gym.DAL.Context
{
    public class DatabaseContext: DbContext
    {
        public DbSet<Exercise> Excercises { get; set; }

        public DbSet<Routine> Routines { get; set; }

        public DbSet<PersonalMax> PersonalMaxes { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
