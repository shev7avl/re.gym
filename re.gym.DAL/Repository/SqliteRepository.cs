﻿using Microsoft.EntityFrameworkCore;
using re.gym.Core.Abstract;
using re.gym.DAL.Context;

namespace re.gym.DAL.Repository
{
    public class SqliteRepository<T>: IRepository<T> where T : BaseEntity
    {
        private readonly DbSet<T> _values;
        private readonly DatabaseContext _context;

        public SqliteRepository(DatabaseContext context)
        {
            _context = context;
            _values = context.Set<T>();
        }

        #region Sync
        public void Add(T entity)
        {
            _values.Add(entity);
            _context.SaveChanges();
        }

        public void AddRange(IEnumerable<T> entities)
        {
            _context.Set<T>().AddRange(entities);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var entity = _values.Where(o => o.Id == id).First();
            _values.Remove(entity);
            _context.SaveChanges();
        }

        public T Get(int id)
        {
            return _values.Find(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _values;
        }

        public IEnumerable<T> GetRange(IEnumerable<int> ids)
        {
            return _values.Where(o => ids.Contains(o.Id));
        }

        public void Update(T entity)
        {
            _values.Update(entity);
            _context.SaveChanges();
        }

        public void UpdateRange(IEnumerable<T> entities)
        {
            _context.UpdateRange(entities);
            _context.SaveChanges();
        }

        #endregion

        #region Async
        public async Task AddAsync(T entity, CancellationToken cancellationToken)
        {
            try
            {
                await _values.AddAsync(entity, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        public async Task AddRangeAsync(IEnumerable<T> entities, CancellationToken cancellationToken)
        {
            await _values.AddRangeAsync(entities, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteAsync(int id, CancellationToken cancellationToken)
        {
            var entity = await _values.FirstAsync(e => e.Id == id, cancellationToken);
            _values.Remove(entity);
            await _context.SaveChangesAsync(cancellationToken);
        }

        public void DeleteRange(IEnumerable<int> ids)
        {
            var entities = _values.Find(ids);
            _values.RemoveRange(entities);
            _context.SaveChanges();
        }

        public async Task DeleteRangeAsync(IEnumerable<int> ids, CancellationToken cancellationToken)
        {
            var entities = await _values.FindAsync(ids);
            _values.RemoveRange(entities);
            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken)
        {
            return await _values.ToListAsync(cancellationToken);
        }

        public async Task<T> GetAsync(int id, CancellationToken cancellationToken)
        {
            return await _values.FirstAsync(o => o.Id == id, cancellationToken);
        }

        public async Task<IEnumerable<T>> GetRangeAsync(IEnumerable<int> ids, CancellationToken cancellationToken)
        {
            return await _values.Where(o => ids.Contains(o.Id)).ToListAsync(cancellationToken);
        }

        public async Task UpdateAsync(T entity, CancellationToken cancellationToken)
        {
            _values.Update(entity);
            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task UpdateRangeAsync(IEnumerable<T> entities, CancellationToken cancellationToken)
        {
            _values.UpdateRange(entities);
            await _context.SaveChangesAsync(cancellationToken);
        }
        #endregion
    }
}
